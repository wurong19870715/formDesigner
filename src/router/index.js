import Vue             from 'vue'
import VueRouter       from 'vue-router'
import formDesigner            from '@/components/form-designer'
import view            from '@/pages/view'
import table           from '@/pages/table'
import dialogTest      from '@/pages/dialogTest'
import queryDialogTest from '@/pages/queryDialogTest'
import editor          from '@/components/extend/fancyEditor'



Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'formDesigner',
    component: formDesigner
  },{
    path: '/dialog',
    name: 'dialogTest',
    component: dialogTest
  },{
    path: '/queryDialog',
    name: 'queryDialogTest',
    component: queryDialogTest
  },{
    path: '/view',
    name: 'view',
    component: view
  },{
    path: '/table',
    name: 'table',
    component: table
  },{
    path:'/editor',
    name:'editor',
    component: editor
  }
]

const router = new VueRouter({
  routes
})

export default router