import vue     from 'vue'
import axios   from 'axios'
import router  from '@/router'
import App     from './App.vue'
import plugins from './components/index'

import './assets/iconfont/iconfont.js'
import './assets/iconfont/iconfont.css'

//import home  from './index'

vue.config.productionTip = false;
Vue.config.devtools      = true;
Vue.prototype.$axios     = axios
vue.use(plugins);

new vue({
  router,
  render: h => h(App)
}).$mount('#app')
