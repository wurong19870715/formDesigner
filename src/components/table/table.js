import { getSimpleId } from "../utils/IdGenerate";
import   constant      from '../utils/constants'
import { jsonClone   } from "../utils";

export const tdItemObj = {
	id       : '',
	col      : 1,
	row      : 1,
	hide     : false,
	compType : 'tdItem',
	style    : {
		width      : 35,
		background : constant.defaultTdBackgroundColor
	},
	columns  : []
}

export const getTdItem = () => {
	let tdItem = cloneObj(tdItemObj);
	tdItem.id  = getSimpleId();
	return tdItem;
}

export const getTitleTdItem = () => {
	let tdItem              = cloneObj(tdItemObj);
	tdItem.id               = getSimpleId();
	tdItem.style.width      = 15;
	tdItem.style.background = constant.defaultTitleTdBackgroundColor;
	return tdItem;
}

export const cloneObj = (source) => {
	let target = jsonClone(source);
	target.id  = getSimpleId();
	return target;
}

export const getTrItem = () => {
	let trItem = [
		getTitleTdItem(), 
		getTdItem(), 
		getTitleTdItem(), 
		getTdItem()
	];
	return trItem;
}

export const getDefaultTrs = () => {
	let trs    = [
		getTrItem(), 
		getTrItem()
	];
	return trs;
}