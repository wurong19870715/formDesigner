const intformat   = require('biguint-format')
const FlakeId     = require('flake-idgen')


const generator   = new FlakeId({ datacenter: 0, worker: 0 });

export const getSimpleId = ()=>{
  return intformat(generator.next(),'dec')
}

export const setTableId  = (table)=>{
  if(table.layoutArray['length'] && table.layoutArray['length']>0){
    table.layoutArray.forEach((tr,index)=>{
      tr['id']=getSimpleId()
      if(tr['length'] && tr['length']>0){
        tr.forEach(td=>{
          td['id']=getSimpleId()
        })
      }
    })
  }
  return table;
}